import React from 'react';
import PropTypes from 'prop-types';
import './style.less';

const Description = props => {
  return (
    <div className="description">
      <h3 className="title">about me</h3>
      <p className="detail">{props.description}</p>
    </div>
  );
};

Description.propTypes = {
  description: PropTypes.string
};

export default Description;
