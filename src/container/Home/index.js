import React from 'react';
import { getInformation } from '../../api';
import Header from './Header';
import Description from './Description';
import './style.less';
import EducationList from './Educations';

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      person: {},
      description: '',
      educations: []
    };
  }

  componentDidMount() {
    getInformation().then(data => {
      const { person, description, educations } = data;
      this.setState({
        person: person,
        description: description,
        educations: educations
      });
    });
  }

  render() {
    return (
      <div className="home">
        <Header person={this.state.person} />
        <section className="description-wrapper">
          <Description description={this.state.description} />
        </section>
        <section className="educations-wrapper">
          <EducationList educations={this.state.educations} />
        </section>
      </div>
    );
  }
}

export default Home;
